# １つのフィールドに複数の状態を保存するプラグインです

わざわざ別テーブルを用意するほどの種類もないような状態を保存するのに便利です  
また一部一致での検索が容易に行えるようになったりもします

使い方として基本的には下記の設定を行えば動作します  
登録、検索時にはチェックボックスで値を渡せば中で勝手に演算を行います

    class Post extends AppModel{
    
      // MultipleStateプラグインの呼び出し
      public $actsAs = array('MultipleState.MultipleState');
    
      // 複数の情報を保持するフィールドの定義
      public $multipleStateFields = array();
    
      public function __construct($id = false, $table = null, $ds = null){
        parent::__construct($id, $table, $ds);
    
        // コンストラクタで情報を設定
        $this->multipleStateFields = array(
          'category' => Configure::read('category'),
        );
    
        /*
        // Configure::read('category')の中身
        // ビット演算を行うので基本的に1,2,4,8添字を定義する
        // 保存できる最大数はフィールドの容量による
        // mysqlのint型のunsignedであれば32個まで登録可能
        array(
          1  => "RPG",
          2  => "SRPG",
          4  => "格闘",
          8  => "恋愛",
          16 => "STG",
          32 => "ADV",
        )
        */
      }
    
    }

補足：  
検索内容として配列を与えれば部分一致検索  
単一の値を渡せば完全一致検索になります

取得時には中の値を配列に変換し取得し  
追加情報として「フィールド名_names」で設定項目の内容を配列で追加します

ソースコードはご自由にお使いください  
なにかありましたら [作者](https://twitter.com/t_katsura "作者") までお気軽にご連絡ください
