<?php

App::uses('ModelBehavior', 'Model');

class MultipleStateBehavior extends ModelBehavior{

  public function beforeFind(Model $model, $query){
    if( empty($model->multipleStateFields) ){
      return $query;
    }

    foreach($model->multipleStateFields as $key => $value){
      $aliasKey = $model->alias . '.' . $key;
      if( isset($query['conditions'][$aliasKey]) && is_array($query['conditions'][$aliasKey]) ){
        $aliasValue = array_sum($query['conditions'][$aliasKey]);
        array_push($query['conditions'], $aliasKey . ' & ' . $aliasValue . '=' . $aliasValue);
        unset($query['conditions'][$aliasKey]);
      }
    }
    return $query;
  }

  public function afterFind(Model $model, $result, $primary){
    if( empty($model->multipleStateFields) ){
      return $result;
    }

    foreach($model->multipleStateFields as $stateKey => $stateValue){
      krsort($stateValue);
      foreach($result as $resultKey => $resultValue){
        if( isset($resultValue[$model->alias]) && is_array($resultValue[$model->alias]) ){
          $state = $resultValue[$model->alias][$stateKey];
          $fields = array();
          foreach($stateValue as $key => $value){
            if( $state >= $key ){
              $fields[$key] = $value;
              $state -= $key;
            }
          }
          $result[$resultKey][$model->alias][$stateKey] = array_keys($fields);
          $result[$resultKey][$model->alias][$stateKey . '_names'] = array_values($fields);
        }
      }
    }
    return $result;
  }

  public function beforeSave(Model $model){
    if( empty($model->multipleStateFields) ){
      return true;
    }

    foreach($model->multipleStateFields as $key => $value){
      if( isset($model->data[$model->alias][$key]) && is_array($model->data[$model->alias][$key]) ){
        $model->data[$model->alias][$key] = array_sum($model->data[$model->alias][$key]);
      }
    }
    return true;
  }

}

